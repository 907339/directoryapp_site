<?php
	header("Access-Control-Allow-Origin: *");
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

		<!-- jquery, data tables -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
		<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

		<!-- bootstrap --> 
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
 	<body>
 		<div class="col-lg-12">
 			<table id="directory" class="table table-condensed">
 				<thead>
 					<th>Directory</th>
 				</thead>
 				<tbody>
 					<tr>
 						<td id="root"></td>
 					</tr>
 				</tbody>
 			</table>
 		</div>

		<script>

			// initialize data table
			$('#directory').dataTable({
				'paging': false,
				'bInfo': false
			});

			// initial ajax page load to get the current state of the directory object
			$.ajax({
				url: 'http://52.14.219.222:5000/showdirectory',
				type: 'GET',
				async: true,
				dataType: 'json', 
				error: function(request, error){
					console.log("request", request)
					console.log("error", error)
					alert('Error contacting API.');
				}, 
				success: function (data){
					console.log(data)
					$("#root").text(build_directory(data))
				}
			});  

			function build_directory(dir){
				return dir[0]['name']
			}
		</script>
 	</body>
</html>